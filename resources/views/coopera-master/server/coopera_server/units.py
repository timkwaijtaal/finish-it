from random import randint

commands = {
    'up': (-1, 0),
    'down': (1, 0),
    'left': (0, -1),
    'right': (0, 1)
}


def addVectors(a, b):
    return (a[0] + b[0], a[1] + b[1])


def subtractVectors(a, b):
    return (a[0] - b[0], a[1] - b[1])


def divVectors(a, b):
    if a[0] == 0 and b[0] == 0:
        c0 = 0
    else:
        c0 = a[0] / b[0]
    if a[1] == 0 and b[1] == 0:
        c1 = 0
    else:
        c1 = a[1] / b[1]
    return (round(c0), round(c1))


def absVector(a):
    return (abs(a[0]), abs(a[1]))


def baseVector(a):
    return divVectors(a, absVector(a))


class Unit(object):

    def __init__(self, _id):
        self.id = _id
        self.type = 'unit'
        # If this unit denies the player to win at this turn.
        self.denies_victory = False
        # If this unit forces the player to lose at this turn.
        self.forces_defeat = False
        # If other units can pass over it.
        self.impassable = True
        # If was removed from the map
        self.removed = False

    def to_json(self):
        return {
            'id': self.id,
            'type': self.type,
            'removed': self.removed
        }

    def run(self, context: dict):
        '''
        Will be called for each unit in each turn.
        '''
        pass

    def touched(self, unit: object, _map: object):
        '''
        Will be called when another unit touches this unit.
        '''
        pass


class Player(Unit):

    def __init__(self, _id):
        super().__init__(_id)
        self.type = 'player'

    def run(self, context: dict):
        current_pos = context['pos']
        _map = context['map']
        new_pos = current_pos
        for player in context['players']:
            if _map.check_lost():
                return

            command = player.get_command()
            if command:
                tmp_pos = addVectors(new_pos, commands[command])
                valid = _map.is_valid_pos(tmp_pos)
                if valid:
                    new_pos = tmp_pos
                    _map.add_history('player', {
                        'pos': tmp_pos,
                        'command': command,
                        'player_name': player.get_name(),
                        'valid': valid
                    })

                    # Touch units while passing or trying to pass over them
                    _map.touch_pos(tmp_pos, self)

            # update position if needed
            if new_pos != current_pos:
                _map.move(current_pos, new_pos, self)
                current_pos = new_pos


class Capturable(Unit):

    def __init__(self, _id):
        super().__init__(_id)
        self.type = 'capture'
        self.denies_victory = True
        self.impassable = False

    def touched(self, unit: object, _map: object):
        if isinstance(unit, Player):
            _map.remove_element(self)
            _map.add_history('capture')


class Dangerous(Unit):

    def __init__(self, _id):
        super().__init__(_id)
        self.type = 'danger'
        self.walking = True

    def touched(self, unit: object, _map: object):
        if isinstance(unit, Player):
            self.forces_defeat = True
            self.walking = False

    def move(self, current_pos, new_pos, _map):
        '''
        Moves possibily defeating the player if touches it.
        '''
        units_in_pos = _map.get_units_pos(new_pos)
        if not units_in_pos:
            _map.move(current_pos, new_pos, self)
        elif len(units_in_pos) == 1 and isinstance(units_in_pos[0], Player):
            _map.move(current_pos, new_pos, self)
            self.forces_defeat = True


class Wanderer(Dangerous):

    def run(self, context: dict):
        if self.walking:
            # Wanders randomly defeating the player if touches it.
            _map = context['map']
            current_pos = context['pos']
            new_pos = _map.rand_pos(
                area=current_pos+(1,), empty=False, passable=False)
            self.move(current_pos, new_pos, _map)


class Hunter(Dangerous):

    def run(self, context: dict):
        if self.walking:
            _map = context['map']
            current_pos = context['pos']
            player_pos = _map.get_player_pos()
            diff_pos = subtractVectors(player_pos, current_pos)
            dir_vector = baseVector(diff_pos)

            # if would move in both axies, randomly moves in both or only one
            if dir_vector[0] and dir_vector[1]:
                index = randint(0, 2)
                if index == 0:
                    dir_vector = (0, dir_vector[1])
                elif index == 1:
                    dir_vector = (dir_vector[0], 0)
            new_pos = addVectors(current_pos, dir_vector)
            self.move(current_pos, new_pos, _map)
