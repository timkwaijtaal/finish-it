#!/usr/bin/env python

from coopera_server.app import app, socketio
# Must import to load API
from coopera_server import api

if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0')
